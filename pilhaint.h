#include <stdio.h>
#define tamanho 15

typedef struct Pilha{
  int itens[tamanho];
  int topo;
}Pilha;

void iniciarPilha(Pilha *p){
  p->topo = -1;
};

void mostrarTopo(Pilha *p){
    printf("Topo da pilha: %d \n", p->itens[p->topo]);
}

void push(Pilha *p, int val){
  
  (*p).topo = p->topo+1;
  (*p).itens[p->topo] = val;
  printf("Numero %i Adicionado\n", val);
}

void pop(Pilha *p){
  
  printf("Numero %i removido \n", (*p).itens[p->topo]);
  (*p).topo--;
}